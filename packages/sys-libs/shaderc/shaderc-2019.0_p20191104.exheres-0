# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google tag=6c32432cd9571ba52c32c53e7158879b393334e3 ] \
    cmake

SUMMARY="Collection of tools, libraries, and tests for Vulkan shader compilation"
DESCRIPTION="
A collection of tools, libraries and tests for shader compilation. At the moment it includes:
* glslc, a command line compiler for GLSL/HLSL to SPIR-V, and
* libshaderc, a library API for doing the same.
Shaderc wraps around core functionality in glslang and SPIRV-Tools. Shaderc aims to to provide:
* a command line compiler with GCC- and Clang-like usage, for better integration with build systems
* an API where functionality can be added without breaking existing clients
* an API supporting standard concurrency patterns across multiple operating systems
* increased functionality such as file #include support
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-libs/spirv-headers
    build+run:
        dev-lang/glslang[>=7.13.3496]
        dev-lang/spirv-tools
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/21c8be385b3fab5edcb934a6d99f69fd389c4e67.patch
    "${FILES}"/shaderc-2019.0-drop-third-party-code.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DENABLE_CODE_COVERAGE:BOOL=FALSE
    -DSHADERC_ENABLE_NV_EXTENSIONS:BOOL=TRUE
    -DSHADERC_ENABLE_SPVC:BOOL=FALSE
    -DSHADERC_SKIP_INSTALL:BOOL=FALSE
    -DSHADERC_SKIP_TESTS:BOOL=TRUE
)

src_prepare() {
    cmake_src_prepare

    # required when not building from git
    local spirv_tools_ver=$(best_version dev-lang/spirv-tools)
    local glslang_ver=$(best_version dev-lang/glslang)
    edo cat > glslc/src/build-version.inc << EOF
"${PNV}\n"
"${spirv_tools_ver%%:*}\n"
"${glslang_ver%%:*}\n"
EOF
}

