# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-Tools tag=v${PV} ] \
    cmake

SUMMARY="Vulkan Utilities and Tools"
DESCRIPTION="
Vulkan tools and utilities that can assist development by enabling developers to verify their
applications correct use of the Vulkan API.
* VulkanInfo
"
HOMEPAGE+=" https://www.khronos.org/vulkan"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( X wayland ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-libs/vulkan-headers[>=$(ever range 1-3)]
        virtual/pkg-config
    build+run:
        sys-libs/vulkan-loader[>=$(ever range 1-3)]
        X? (
            x11-libs/libxcb
            x11-libs/libX11
        )
        wayland? ( sys-libs/wayland )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DVulkanRegistry_DIR:PATH=/usr/share/vulkan/registry
    -DBUILD_CUBE:BOOL=FALSE
    -DBUILD_ICD:BOOL=FALSE
    -DBUILD_VULKANINFO:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    "X WSI_XCB_SUPPORT"
    "X WSI_XLIB_SUPPORT"
    "wayland WSI_WAYLAND_SUPPORT"
)

