(
    app-crypt/kbfs[~scm]
    app-crypt/keybase[~scm]
    app-text/shared-desktop-ontologies[~scm]
    app-text/zathura[~scm]
    compositor/sway[~scm]
    compositor/weston[~scm]
    media/pipewire[~scm]
    media-sound/pasystray[~scm]
    media-sound/pulseaudio[~scm]
    net-apps/NetworkManager[~scm]
    net-im/folks[~scm]
    net-im/telepathy-accounts-signon[~scm]
    net-misc/mobile-broadband-provider-info[~scm]
    net-wireless/ModemManager[~scm]
    net-www/dwb[~scm]
    sys-auth/elogind[~scm]
    sys-boot/plymouth[~scm]
    sys-libs/libinput[~scm]
    sys-libs/wayland[~scm]
    sys-libs/wayland-protocols[~scm]
    sys-libs/wlc[~scm]
    sys-libs/wlroots[~scm]
    text-plugins/zathura-cb[~scm]
    text-plugins/zathura-djvu[~scm]
    text-plugins/zathura-pdf-mupdf[~scm]
    text-plugins/zathura-pdf-poppler[~scm]
    text-plugins/zathura-ps[~scm]
    x11-apps/i3lock[~scm]
    x11-libs/girara[~scm]
    x11-libs/i3ipc-glib[~scm]
    x11-plugins/i3status[~scm]
    x11-wm/awesome[~scm]
    x11-wm/i3[~scm]
    wayland-apps/swaylock[~scm]
) [[
    *author = [ Paul Seidler <sepek@exherbo.org> ]
    *date = [ 24 Feb 2012 ]
    *token = scm
    *description = [ Mask scm version ]
]]

net-www/chromium-stable[<79.0.3945.79] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 11 Dec 2019 ]
    token = security
    description = [
                    CVE-2019-137{{25..30},32,{34..59},{61..64}}
                  ]
]]

dev-libs/nss[<3.38] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jun 2018 ]
    token = security
    description = [ CVE-2018-0495 ]
]]

net-im/telepathy-idle[<0.1.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 May 2013 ]
    token = security
    description = [ CVE-2007-6746 ]
]]

(
    net-im/telepathy-gabble[<0.16.6]
    net-im/telepathy-gabble[>0.17&<0.17.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 May 2013 ]
    *token = security
    *description = [ CVE-2013-1431 ]
]]

sys-auth/polkit:1[<0.116] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Nov 2019 ]
    token = security
    description = [ CVE-2018-19788, CVE-2019-6133 ]
]]

dev-libs/nspr[<4.10.10] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2015-7183 ]
]]

voip/mumble[<1.2.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2014 ]
    token = security
    description = [ CVE-2014-375{5,6} ]
]]

sys-apps/udisks:2[<2.8.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Oct 2018 ]
    token = security
    description = [ CVE-2018-17336 ]
]]

x11-apps/xdg-utils[<1.1.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 May 2018 ]
    token = security
    description = [ CVE-2017-18266 ]
]]

net-apps/NetworkManager[<1.0.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Apr 2016 ]
    token = security
    description = [ CVE-2016-0764 ]
]]

net-www/firefox[<70.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Dec 2019 ]
    token = security
    description = [ CVE-2019-{11745,11756,13722,17005,17008,17009,17010,17011,
                        17012,17013,17014} ]
]]

mail-client/thunderbird[<52.9.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Jun 2018 ]
    token = security
    description = [ CVE-2018-{5188,12359,12360,12362,12363,12364,12365,12366,
                              12368,12372,12373,12374} ]
]]

mail-libs/libetpan[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 9 May 2017 ]
    token = security
    description = [ CVE-2017-8825 ]
]]

sys-apps/flatpak[<1.2.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Mar 2019 ]
    token = security
    description = [ CVE-2019-8308 ]
]]

net-apps/NetworkManager[>=1.11&<1.12] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 16 May 2018 ]
    token = testing
    description = [ Mask unstable versions of NetworkManager ]
]]

net-dns/avahi[<0.7-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 May 2019 ]
    token = security
    description = [ CVE-2017-6519, CVE-2018-100084 ]
]]
