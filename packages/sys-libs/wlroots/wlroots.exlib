# Copyright 2018-2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='swaywm' ] meson

SUMMARY="A modular Wayland compositor library"
DESCRIPTION="Pluggable, composable modules for building a Wayland compositor"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    caps [[ description = [ for capability support ] ]]
    rdp [[ description = [ Enable support for the RDP backend ] ]]
    X [[ description = [ Enable X11 backend ] ]]
    xwayland [[
        description = [ Enable XWayland (X11 compatibility layer) ]
        requires = [ X ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: elogind systemd-logind ) [[
        *description = [ Logind support ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/libinput[>=1.9.0]
        sys-libs/wayland[>=1.16]
        sys-libs/wayland-protocols[>=1.17]
        x11-dri/mesa[>=17.1.0][wayland][X?]
        x11-dri/libdrm[>=2.4.95]
        x11-libs/libxkbcommon[wayland][X?]
        x11-libs/pixman:1
        caps? ( sys-libs/libcap )
        rdp? ( net-remote/FreeRDP[>=2.0] )
        X? (
            x11-libs/libxcb
            x11-proto/xcb-proto
            x11-utils/xcb-util-image
            x11-utils/xcb-util-renderutil
            x11-utils/xcb-util-wm
        )
        providers:elogind? ( sys-auth/elogind[>=237] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        providers:systemd-logind? ( sys-apps/systemd[>=237] )
    run:
        X? (
            x11-server/xorg-server[xwayland?]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
    -Dlogind=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'caps libcap'
    'rdp freerdp'
    'X x11-backend'
    'X xcb-icccm'
    'xwayland xwayland'
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dlogind=enabled'
    'providers:elogind -Dlogind-provider=elogind'
    'providers:systemd-logind -Dlogind=enabled'
    'providers:systemd-logind -Dlogind-provider=systemd'
)

