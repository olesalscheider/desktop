# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PV##*_p}
UBUNTU_RELEASE=16.10

require launchpad autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A set of symbols and convience functions that all indicators would like to use"
DOWNLOADS="https://bazaar.launchpad.net/~indicator-applet-developers/${PN}/trunk.${UBUNTU_RELEASE}/tarball/${MY_PNV} -> ${PNV}.tar.gz"

BUGS_TO="keruspe@exherbo.org"

LICENCES="GPL-3"
SLOT="0.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk2"

# Needs X
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.37]
        x11-libs/gtk+:3[>=3.6]
        gtk2? ( x11-libs/gtk+:2[>=2.18] )
"

WORK=${WORKBASE}/\~indicator-applet-developers/${PN}/trunk.${UBUNTU_RELEASE}

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-revert-r495.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --disable-tests
)

src_prepare() {
    edo sed -e 's/-Werror//' \
            -i tests/Makefile.am \
            -i libindicator/Makefile.am

    autotools_src_prepare
}

src_configure() {
    if option gtk2; then
        edo mkdir "${WORKBASE}"/gtk2-build
        edo cp -r "${WORK}"/* "${WORKBASE}"/gtk2-build
        edo pushd "${WORKBASE}"/gtk2-build

        local myconf=( "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" )
        myconf+=( --with-gtk=2 )
        econf ${myconf[@]}

        edo popd
    fi

    default
}

src_compile() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

src_install() {
    if option gtk2; then
        edo pushd "${WORKBASE}"/gtk2-build
        default
        edo popd
    fi

    default
}

