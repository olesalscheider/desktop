# Copyright 2007-2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011, 2012, 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${MY_PV:-${PV/_beta/b}}"
MY_PV="${MY_PV/_rc/rc}"

# /mozilla-*/browser/branding/unofficial/locales/en-US/brand.properties
MOZ_CODENAME="Mozilla Developer Preview"

# firefox-${PV}/browser/locales/shipped-locales
linguas=(
    ach af an ar ast az be bg bn br bs ca_valencia ca cak cs cy da de dsb el
    en_CA en_GB en_US eo es_AR es_CL es_ES es_MX et eu fa ff fi fr fy_NL ga_IE
    gd gl gn gu_IN he hi_IN hr hsb hu hy_AM ia id is it ja ka kab kk km kn ko
    lij lt lv mk mr ms my nb_NO ne_NP nl nn_NO oc pa_IN pl pt_BR pt_PT rm ro
    ru si sk sl son sq sr sv_SE ta te th tl tr trs uk ur uz vi xh zh_CN zh_TW
)

# autoconf:2.5 doesn't work - https://bugzilla.mozilla.org/show_bug.cgi?id=104642
require autotools [ supported_autoconf=[ 2.1 ] supported_automake=[ 1.16 1.15 ] ]
require mozilla-pgo [ co_project=browser codename="${MOZ_CODENAME}" ]
require freedesktop-desktop
require toolchain-funcs
require utf8-locale

export_exlib_phases pkg_setup src_unpack src_prepare src_configure src_install

SUMMARY="Mozilla's standalone web browser"
HOMEPAGE="https://www.mozilla.com/en-US/${PN}"
DOWNLOADS="
    https://ftp.mozilla.org/pub/${PN}/releases/${MY_PV}/source/${PN}-${MY_PV}.source.tar.xz
"
for lang in "${linguas[@]}" ; do
    DOWNLOADS+="
        linguas:${lang}? ( https://ftp.mozilla.org/pub/${PN}/releases/${MY_PV}/linux-i686/xpi/${lang/_/-}.xpi -> ${PN}-${MY_PV}-${lang}.xpi )
    "
done

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/${MY_PV}/releasenotes"

LICENCES="
    || ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )
"
SLOT="0"
MYOPTIONS="
    alsa dbus
    eme       [[ description = [ Enable Encrypted Media Extensions, a form of DRM used for sites like Netflix ] ]]
    jack      [[ description = [ Enable JACK backend ] ]]
    libproxy  [[ description = [ Use libproxy for system proxy settings ] ]]
    lto       [[ description = [ Enable cross-language (c++ + rust) link-time optimisations ] ]]
    pulseaudio
    startup-notification [[ description = [ Enables user and system feedback during startup ] ]]
    wayland   [[ description = [ Use wayland as the default backend ] ]]

    ( libc: musl )
    ( linguas: ${linguas[@]} )
    ( providers: jpeg-turbo )
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/clang:*
        dev-lang/nasm
        dev-lang/perl:*[>=5.6]
        dev-lang/python:2.7[sqlite]
        dev-lang/python:*[>=3][sqlite]
        dev-lang/yasm[>=1.3]
        virtual/pkg-config[>=0.9.0]
        virtual/unzip
        lto? ( sys-devel/lld )
    build+run:
        app-spell/hunspell:*
        dev-lang/llvm:=[>=3.9]
        dev-lang/node
        dev-lang/rust:*[>=1.37.0]
        dev-libs/atk
        dev-libs/glib:2[>=2.26]
        dev-libs/icu:=[>=64.1]
        dev-libs/libIDL:2[>=0.8.0]
        dev-libs/libevent:=
        dev-libs/libffi[>=3.0.10]
        dev-libs/nspr[>=4.23]
        dev-libs/nss[>=3.47.1]
        dev-rust/cbindgen[>=0.9.1]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.1.0]
        media-libs/libwebp[>=1.0.2]
        x11-dri/mesa
        x11-libs/cairo[>=1.10.2-r1][X] [[ note = [ required for tee backend ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2[>=2.18]
        x11-libs/gtk+:3[>=3.10.0][wayland]
        x11-libs/libICE
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libxkbcommon[>=0.4.1]
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/pango[>=1.22.0]
        x11-libs/pixman:1[>=0.19.2]
        alsa? (
            sys-sound/alsa-lib
        )
        dbus? ( sys-apps/dbus[>=0.60]
                dev-libs/dbus-glib:1[>=0.60] )
        jack? ( media-sound/jack-audio-connection-kit )
        libproxy? ( net-libs/libproxy:1[-webkit] ) [[ note = [ can't mix gtk3 and gtk2 symbols ] ]]
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
        startup-notification? ( x11-libs/startup-notification[>=0.8] )
    suggestion:
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"
# We currently don't built against system libvpx, see below
#        media-libs/libvpx:=[>=1.7.0]

# Do not use system sqlite since it requires secure_delete to be enabled by default.
# This incurs a preformance penalty for zeroing data on delete.
# There is a patch to make xulrunner do a PRAGMA secure_delete = ON;
# at runtime to avoid the overhead on other programs. But upstream rejected it.
# see: https://bugzilla.mozilla.org/show_bug.cgi?id=546162 and
# https://bugzil.la/1049920
#
# If we decide to accept this penalty or decide
# to use the unsupported patch then we will also need:
#
# threadsafe: already enabled in the exheres
# unlock-notify: already an option in the exheres

MOZILLA_SRC_CONFIGURE_PARAMS=(
    --disable-crashreporter
    --disable-necko-wifi
    --disable-openmax
    --disable-synth-speechd # seems dependencyless but dlopens libspeechd.so.2
    --disable-system-sqlite
    --disable-updater
    --disable-warnings-as-errors
    --enable-chrome-format=omni
    --enable-hardening
    --enable-optimize
    --enable-rust-simd
    --enable-sandbox
    --enable-system-ffi
    --enable-system-pixman
    --enable-webrtc
    --with-intl-api
    --with-system-bz2
    --with-system-icu
    --with-system-libevent
    # --with-system-libvpx FIXME: doesn't support system libvpx 1.8
    --with-system-webp
    --with-system-zlib
    # "Obsolete: do not use --with-system-cairo"
    --without-system-cairo
    --without-system-png # Requires patches for APNG support, which we will not support
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    'libc:musl --disable-jemalloc --enable-jemalloc'
    'eme --enable-eme=widevine --disable-eme'
)

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    dbus
    jack
    libproxy
    pulseaudio
    startup-notification
)
MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

firefox_pkg_setup() {
    require_utf8_locale

    if optionq lto; then
        cc-is-clang && ld-is-lld || die "You need to use clang and lld to enable cross-languages LTO"
    fi
}

firefox_src_unpack() {
    default

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            edo mkdir "${WORKBASE}"/${lang}
            edo unzip -qo "${FETCHEDDIR}"/${PN}-${MY_PV}-${lang}.xpi -d "${WORKBASE}"/${lang}
        fi
    done
}

old-eautoreconf() {
    local backup=( configure.in )
    edo mv "${backup[@]}" "${TEMP}"
    edo cp {old-,}configure.in
    eautoreconf
    edo mv {,old-}configure
    edo mv "${backup[@]/#/${TEMP}/}" .
}

firefox_src_prepare() {
    mozilla-pgo_src_prepare

    # untill fully switched to the new python build system
    old-eautoreconf
    edo pushd js/src >/dev/null
    old-eautoreconf
    edo popd >/dev/null
}

firefox_src_configure() {
    # x86_64-pc-linux-gnu-as: invalid option -- 'N'
    export AS=$(exhost --tool-prefix)cc
    export MOZBUILD_STATE_PATH="${TEMP}".mozbuild
    # NOTE(somasis): fix library loading on musl
    # NOTE(woutershep): do not apply to glibc, breaks PGO.
    libc-is-musl && append-flags "-Wl,-rpath=/usr/$(exhost --target)/lib/${PN}"

    # Disable elf-hack for now because of https://bugzilla.mozilla.org/show_bug.cgi?id=1482204
    mozilla-pgo_src_configure \
        $(cc-is-clang || echo "--with-clang-path=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)clang") \
        --disable-elf-hack \
        $(option lto && echo "--enable-lto=cross") \
        --enable-default-toolkit=cairo-gtk3$(option wayland -wayland)
}

firefox_src_install() {
    mozilla-pgo_src_install

    # some resources (ex: jsloader, jssubloader) are put into ommi-jar so the directory is empty
    edo find "${IMAGE}" -type d -empty -delete

    edo hereenvd 50firefox <<EOF
COLON_SEPARATED="MOZ_PLUGIN_PATH"
MOZ_PLUGIN_PATH="/usr/$(exhost --target)/lib/nsbrowser/plugins:/opt/nsbrowser/plugins"
EOF

    # allow installation of distributed extensions and read/use system locale on runtime
    insinto /usr/$(exhost --target)/lib/${PN}/browser/defaults/preferences
    hereins all-exherbo.js <<EOF
pref("extensions.autoDisableScopes", 3);
pref("intl.locale.requested", "");
EOF

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            # Extract an id from the localisation tarball, usually something
            # like langpack-${lang}@firefox.mozilla.org. Not entirely sure
            # which rules it follows so I go with cargo-cult for now.
            emid="$(sed -n -e 's/.*"id": "\(.*\)",/\1/p' "${WORKBASE}"/${lang}/manifest.json | xargs)"
            insinto /usr/$(exhost --target)/lib/${PN}/browser/extensions
            newins "${FETCHEDDIR}"/${PN}-${MY_PV}-${lang}.xpi ${emid}.xpi
        fi
    done
}

